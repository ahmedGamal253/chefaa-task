//
//  HomeViewModel.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/5/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MOLH

class HomeViewModel{
    var showLoader = BehaviorRelay(value: false)
    var hideViewData = BehaviorRelay(value: false)
    var sliderItems = PublishSubject<[String]>()
    var offersItems = PublishSubject<[LandingPageModel]>()
    var categoriesItem = PublishSubject<[SliderModel]>()
    var brandsItem = PublishSubject<[BrandModel]>()
    var sellingItems = PublishSubject<[BestSellingModel]>()
    var HiddenError = BehaviorRelay(value: false)
    var hightCategories = BehaviorRelay(value: CGFloat())
    var textLanguage = BehaviorRelay(value: "ع")
    
    func getData(){
        HiddenError.accept(true)
        hideViewData.accept(true)
        showLoader.accept(true)
        AppConnectionsHandler.get(url: AppUrl.instance.getHomeUrl(), headers: GET_DEFAULT_HEADERS(), type: HomeResponseModel.self) { [weak self] (status, model, error) in
            guard let self = self else {return}
            self.showLoader.accept(false)
            switch status {
            case .sucess:
                self.hideViewData.accept(false)
                let model = model as! HomeResponseModel
                self.filterSliderImages(sliders: model.data.slider)
                self.offersItems.onNext(model.data.landingPages)
                self.categoriesItem.onNext(model.data.subCategories)
                self.getCategoriesHight(count: model.data.subCategories.count)
                self.brandsItem.onNext(model.data.brands)
                self.sellingItems.onNext(model.data.bestselling)
                break
            case .errorFromServer:
                self.HiddenError.accept(false)
                break
            case .errorFromInternet:
                self.HiddenError.accept(false)
                break
            }
        }
    }
    
    private func filterSliderImages(sliders:[SliderModel]){
        sliderItems.onNext( sliders.map{"https://test.eldev.tech/\($0.image)"})
    }
    
    func getItemSize(collection:String)->CGSize{
        print(collection)
        switch collection {
        case "collectionOffers":
            return CGSize(width: GET_RATIO(120), height: GET_RATIO(80))
        case "collectionCategories":
            return getCategoriesCollectionItemSize()
        case "collectionBrands":
            return CGSize(width: GET_RATIO(90), height: GET_RATIO(80))
        case "collectionBestSelling":
            return CGSize(width: GET_RATIO(160), height: GET_RATIO(215))
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    
    private func getCategoriesCollectionItemSize()->CGSize{
        let widthCell = (UIScreen.main.bounds.width - GET_RATIO(30)) / 5
        let hightCell = widthCell * 1.3
        return CGSize(width: widthCell, height: hightCell )
    }
    
    private func getCategoriesHight(count:Int){
        var row = 0
        let restDivision = count % 5
        restDivision == 0 ? (row = count/5) : (row = ((count - restDivision) / 5) + 1)
        let hightCell = ((UIScreen.main.bounds.width - GET_RATIO(30)) / 5) * 1.3
        hightCategories.accept(hightCell * CGFloat(row))
    }
    
    func getlanguageText(){
        if  MOLHLanguage.currentAppleLanguage() == "en" {
            textLanguage.accept("ع")
        }else{
            textLanguage.accept("En")
        }
    }
    
    func changeLanguage(){
        if  MOLHLanguage.currentAppleLanguage() == "en" {
            MOLH.setLanguageTo("ar")
        } else if MOLHLanguage.currentAppleLanguage() == "ar" {
            MOLH.setLanguageTo("en")
        }
        MOLH.reset(transition: .transitionCrossDissolve)
    }
}
