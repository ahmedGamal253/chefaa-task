//
//  HomeExtensions.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/6/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.getItemSize(collection: collectionView.restorationIdentifier ?? "")
    }
}
