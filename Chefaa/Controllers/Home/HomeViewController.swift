//
//  HomeViewController.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/5/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import UIKit
import PHCyclePictureView
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
    
    @IBOutlet weak var constrantHightCollectionCategories: CustomConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var collectionBestSelling: UICollectionView!
    @IBOutlet weak var collectionBrands: UICollectionView!
    @IBOutlet weak var collectionCategories: UICollectionView!
    @IBOutlet weak var collectionOffers: UICollectionView!
    @IBOutlet weak var viewPager: PHCyclePictureView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var btnLang: UIButton!
    
    let viewModel = HomeViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getlanguageText()
        subscribeToResponse()
        subscribeToSlider()
        subscribeToCollectionOffers()
        subscribeToCollectionCategories()
        subscribeToCollectionSelling()
        subscribeToCollectionBrands()
        subscribeToBtnLang()
        viewModel.getData()
        initUi()
        // Do any additional setup after loading the view.
    }
    
    private func initUi(){
        viewSearch.layer.cornerRadius = GET_RATIO(17.5)
    }
    
    private func subscribeToResponse(){
        viewModel.showLoader.subscribe(onNext: { (show) in
            self.showIndicator(show)
        }).disposed(by: disposeBag)
        viewModel.HiddenError.bind(to: viewError.rx.isHidden).disposed(by: disposeBag)
        viewModel.hideViewData.bind(to: scrollView.rx.isHidden).disposed(by: disposeBag)
    }
    
    private func subscribeToSlider(){
        viewModel.sliderItems.subscribe(onNext: { (images) in
            self.initPager(images: images)
        }).disposed(by: disposeBag)
    }
    
    private func subscribeToCollectionOffers(){
        viewModel.offersItems.bind(to: collectionOffers.rx.items(cellIdentifier: "OffersCollectionViewCell", cellType: OffersCollectionViewCell.self)){ row,data,cell in
            cell.configCell(data)
        }.disposed(by: disposeBag)
    }
    
    private func subscribeToCollectionCategories(){
        viewModel.hightCategories.bind(to: constrantHightCollectionCategories.rx.constant).disposed(by: disposeBag)
        viewModel.categoriesItem.bind(to: collectionCategories.rx.items(cellIdentifier: "CategoryCollectionViewCell", cellType: CategoryCollectionViewCell.self)){ row,data,cell in
            cell.configCell(data)
        }.disposed(by: disposeBag)
    }
    
    private func subscribeToCollectionSelling(){
        viewModel.sellingItems.bind(to: collectionBestSelling.rx.items(cellIdentifier: "ProductCollectionViewCell", cellType: ProductCollectionViewCell.self)){ row,data,cell in
            cell.configCell(data)
        }.disposed(by: disposeBag)
    }
    
    private func subscribeToCollectionBrands(){
        viewModel.brandsItem.bind(to: collectionBrands.rx.items(cellIdentifier: "BrandCollectionViewCell", cellType: BrandCollectionViewCell.self)){ row,data,cell in
            cell.configCell(data)
        }.disposed(by: disposeBag)
    }
    
    
    private func subscribeToBtnLang(){
        viewModel.textLanguage.bind(to: btnLang.rx.title()).disposed(by: disposeBag)
        btnLang.rx.tap.subscribe(onNext: { (_) in
            self.viewModel.changeLanguage()
        }).disposed(by: disposeBag)
    }
    
    
    private func initPager(images:[String]) {
        viewPager.layer.cornerRadius = GET_RATIO(15)
        viewPager.imagePaths = images
        viewPager.placeholderImage = #imageLiteral(resourceName: "placeholder-1200x500")
        viewPager.anchorBackgroundColor = .clear
        viewPager.currentDotColor = #colorLiteral(red: 0, green: 0.8260638118, blue: 0.5592608452, alpha: 1)
        viewPager.dotColor = .white
        viewPager.contentMode = .scaleAspectFit
        viewPager.clipsToBounds = true
        if images.count > 1 {
            viewPager.isAutoScroll = true
        } else {
            viewPager.isAutoScroll = false
        }
    }
}
