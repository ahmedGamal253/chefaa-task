//
//  BrandCollectionViewCell.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/6/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import UIKit

class BrandCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imageBrand: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         viewCell.setBorder(color:  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), radius: GET_RATIO(8), borderWidth: 1)
    }
    
    func configCell(_ model:BrandModel){
        imageBrand.loadFromUrl(url: "https://test.eldev.tech/\(model.images[0])")
    }
}
