//
//  CategoryCollectionViewCell.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/6/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageCategory: UIImageView!
    
    func configCell(_ model:SliderModel){
        imageCategory.loadFromUrl(url: "https://test.eldev.tech/\(model.image)")
        labelName.text = model.title
    }
}
