//
//  ProductCollectionViewCell.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/6/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var labelOldPrice: UILabel!
    @IBOutlet weak var btnAddCart: UIButton!
    @IBOutlet weak var labelNewPrice: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var labelSave: UILabel!
    @IBOutlet weak var viewSave: UIView!
    
    override func awakeFromNib() {
        viewSave.layer.cornerRadius = GET_RATIO(5)
        btnAddCart.layer.cornerRadius = GET_RATIO(10)
        viewCell.setBorder(color:  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), radius: GET_RATIO(8), borderWidth: 1)
    }
    
    func configCell(_ model:BestSellingModel){
        if model.coupon_description == nil{
            viewSave.isHidden = true
        }else{
            viewSave.isHidden = false
            labelSave.text = model.coupon_description
        }
        imageProduct.loadFromUrl(url: "https://test.eldev.tech/\(model.images[0])")
        labelName.text = model.title
        labelNewPrice.text = "\(model.final_discount ?? 0) EGP"
        // labelOldPrice.text = "\(model.price ?? 0) EGP"
        labelOldPrice.setStrikethrough(text: "\(model.price ?? 0) EGP")
    }
    
    
}

