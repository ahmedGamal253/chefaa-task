//
//  OffersCollectionViewCell.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/6/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import UIKit

class OffersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageOffer: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageOffer.layer.cornerRadius = GET_RATIO(10)
    }
    
    func configCell(_ model:LandingPageModel){
        imageOffer.loadFromUrl(url: "https://test.eldev.tech/\(model.image)")
    }
}
