//
//  AppDelegate.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/5/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import UIKit
import MOLH

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        initMOLH()
        return true
    }
    
    func initMOLH() {
        MOLHLanguage.setDefaultLanguage("en")
        MOLH.shared.activate(true)
    }
    // MARK: UISceneSession Lifecycle
    
    
}

extension AppDelegate: MOLHResetable {
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "HomeViewController")
    }
    
}

