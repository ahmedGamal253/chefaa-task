//
//  HomeResponseModel.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/5/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import Foundation

struct HomeResponseModel: Codable {
    let data: DataClass
    let code: Int
}

// MARK: - DataClass
struct DataClass: Codable {
    let slider, subCategories: [SliderModel]
    let brands: [BrandModel]
    let bestselling: [BestSellingModel]
    let landingPages: [LandingPageModel]
    let landing_page_title: String
}

// MARK: - Bestselling
struct BestSellingModel: Codable {
    let id: Int
    let title, title_ar, title_en, slug: String
    let is_favorite: Bool
    let full_url: String
    let quantity: Int
    let verified: String
    let status: String
    let is_outofstock, views: Int
    let is_notified: Bool
    let pharmacy_branch_id: Int?
    let purchase_count: Int
    let discount: Int?
    let brands: BrandModel?
    let discount_type: String?
    let sub_category_id: Int
    let sub_category_title, sub_category_name, sub_category_name_en, sub_category_name_ar: String
    let main_category_id: Int
    let category_name_en: String?
    let category_name_ar: String
    let brand: Int?
    let images: [String]
    let coupon_description: String?
    let bestsellingDescription, description_ar, description_en: String?
    let price, final_discount: Int?
    let coupon_description_ar: String
}

// MARK: - Brand
struct BrandModel: Codable {
    let id: Int
    let title, title_trans, slug: String
    let images: [String]
}


// MARK: - LandingPage
struct LandingPageModel: Codable {
    let id: Int
    let name, description, image, slug: String
    let is_active, is_main: Int
}



// MARK: - Slider
struct SliderModel: Codable {
    let id: Int
    let title, image: String
    let url: String?
    let slug: String?
}
