//
//  Gif.swift
//  SwiftGif
//
//  Created by Arne Bahlo on 07.06.14.
//  Copyright (c) 2014 Arne Bahlo. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {

    public func loadFromUrl(url:String, placeHolder: String = "placeholder-1200x500") {
        let url = URL(string: url)
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url, placeholder:UIImage.init(named: placeHolder), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
}
