//
//  UIViewExtension.swift
//  Pharmacist
//
//  Created by mohamed elmaazy on 7/9/18.
//  Copyright © 2018 Ahmed gamal. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setBorder(color:UIColor, radius:CGFloat, borderWidth:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
    }
    
}

