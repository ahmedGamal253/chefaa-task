//
//  UIViewControllerExtension.swift
//  Pharmacist
//
//  Created by mohamed elmaazy on 7/9/18.
//  Copyright © 2018 Ahmed gamal. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    
    func showIndicator(_ show: Bool = true) {
        if show {
            let size = CGSize(width: GET_RATIO(40), height: GET_RATIO(40))
            startAnimating (size , type: .ballScaleMultiple, color: #colorLiteral(red: 0, green: 0.8260638118, blue: 0.5592608452, alpha: 1), padding: 10)
        } else {
            stopAnimating()
        }
    }
}
