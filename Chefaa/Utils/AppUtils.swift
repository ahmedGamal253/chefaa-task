//
//  AppUtils.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/5/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import Foundation
import UIKit
import MOLH

func GET_RATIO(_ number: CGFloat) -> CGFloat {
    return number * UIScreen.main.bounds.width / 360
}

func GET_DEFAULT_HEADERS() -> [String: String] {
    return ["Content-Type": "application/json",
            "Accept": "application/json",
            "lang": MOLHLanguage.currentAppleLanguage()]
}

