//
//  AppConnectionsHandler.swift
//  Bahya
//
//  Created by mohamed elmaazy on 5/16/18.
//  Copyright © 2018 mohamed elmaazy. All rights reserved.
//

import Foundation
import Alamofire

public enum ResponseStatus {
    case sucess
    case errorFromServer
    case errorFromInternet
}

class AppConnectionsHandler {
    
    fileprivate static func checkConnection() -> Bool {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")!
        return (reachabilityManager.isReachable)
    }
    
    fileprivate static func getRequest(url: String, parameters: [String: Any]?, headers: [String: String]?) -> URLRequest {
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.allHTTPHeaderFields = headers
        if let json = try? JSONSerialization.data(withJSONObject: parameters ?? [:], options: []) {
            request.httpBody = json as Data
        }
        return request
    }
    
    static func get<T: Codable>(url: String, headers: [String:String]? = nil, type: T.Type, completion: ((ResponseStatus, Codable?, String?) -> Void)?) {
        if checkConnection() {
            Alamofire.request(url, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                let result = handlerResponse(response: response, type: T.self)
                completion?(result.0, result.1, result.2)
            }
        } else {
            completion?(.errorFromInternet, nil, nil)
        }
    }
    
   
   
    fileprivate static func handlerResponse<T: Codable>(response: DataResponse<Any>, type: T.Type) -> (ResponseStatus, Codable?, String?) {
        switch(response.result) {
        case .success(_):
            if response.result.value != nil {
                if response.response?.statusCode == 200 {
                    let jsonData = try? JSONSerialization.data(withJSONObject: response.result.value!)
                    let model = try! JSONDecoder().decode(T.self, from: jsonData!)
                    return(.sucess, model, nil)
                } else {
                    let dic = response.result.value! as? [String : Any] ?? [String: Any]()
                    return(.errorFromServer, nil, dic["error_message"] as? String ?? "")
                }
            } else {
                return(.errorFromInternet, nil, nil)
            }
        case .failure(_):
            return(.errorFromInternet, nil, nil)
        }
    }
}
