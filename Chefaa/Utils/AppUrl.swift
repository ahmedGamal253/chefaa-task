//
//  AppUrl.swift
//  Chefaa
//
//  Created by Ahmed Gamal on 10/5/20.
//  Copyright © 2020 chefaa. All rights reserved.
//

import Foundation
//
//  AppUrl.swift
//  MariaTradeSwift
//
//  Created by Mohamed Elmaazy on 2/4/20.
//  Copyright © 2020 Mohamed Elmaazy. All rights reserved.
//

import Foundation

class AppUrl {
    
    static let instance = AppUrl()
    private init () {}
    
    fileprivate let BASE_URL = "https://test.eldev.tech/api/"
    
    func getHomeUrl() -> String {
        "\(BASE_URL)store/home-page"
    }
   
}
